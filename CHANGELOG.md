# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
[comment]: <> (Types of changes: Added, Changed, Deprecated, Removed, Fixed, Optimized, Security)

## Unreleased

## Added
- IGPIMP-2019
  - cache pending promises
  - Convert to class instance (easier configuration & tests)
  - Allow disabling of cleaning thread (required for tests)
  - Add tests & CI

## 1.1.0
### Added
- `set` and `cached` methods have an option to prevent freezing cached object

## 1.0.0
### Added
- Initial release
