module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
    'es2021': true,
    'jest': true,
  },
  'extends': [
    'eslint:recommended',
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 13,
  },
  'rules': {
    'max-len': [
      'error',
      {
        'code': 120,
        'comments': 999,
        'ignoreComments': true,
        'ignoreStrings': true,
        'ignoreTrailingComments': true,
        'ignoreUrls': true,
        'ignoreTemplateLiterals': true,
      },
    ],
  },
};
