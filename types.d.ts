declare namespace IgpLocalCache {
  interface CacheConfig {
    dataExpireHours?: number;
    checkEveryHours?: number;
  }

  class Cache {
    constructor(conf: CacheConfig);
    config: (conf: CacheConfig) => void;
    cached: (fn: Function, name: string, expiresInHours?: Number, freeze?: boolean) => Promise<any>;
    get: (name: string) => any;
    set: (name: string, value: any, expiresInHours?: Number, freeze?: boolean) => Cache;
    invalidate: (name: string) => Cache;
    invalidateAll: (startsWith: string) => Cache;
  }
}

export declare const Cache = new IgpLocalCache.Cache;
