/**
 * Created by domagoj on 12/7/16.
 */
const _ = require('lodash');

// eslint-disable-next-line require-jsdoc
class Cache {
  _cache = {};
  _conf = {};

  /**
   * Create a cache instance
   *
   * @param {Object?} conf
   */
  constructor(conf) {
    this.config(conf);
  }

  /**
   * Set this module parameters.
   *
   * Set checkEveryHours to 0 to disable cleaning timer.
   *
   * @param {{checkEveryHours: number, dataExpireHours: number}} conf Object containing configuration parameters
   */
  config(conf = {}) {
    this._conf = {
      /** How many hours will data last in cache by default */
      dataExpireHours: conf.dataExpireHours ?? 1,
      /** How many hours cleaning thread will clean expired items from cache */
      checkEveryHours: conf.checkEveryHours ?? 0.5,
    };

    this._startCleaningThread();
  }

  /**
   * Get data from cache
   * @param {string} name Data name
   * @return {Object} Cached data
   */
  get(name) {
    const cachedData = this._cache[name];
    if (cachedData === undefined) {
      // data not found
      return undefined;
    }
    if (Date.now() > cachedData.expires) {
      // data expired
      delete this._cache[name];
      return undefined;
    }
    // data found and not expired
    return cachedData.data;
  }

  /**
   * Add data to cache
   * @param {string} name              Name for the data you cache
   * @param {Object} value             Actual Data to cache
   * @param {number?} expiresInHours    Overrides default value
   * @param {boolean?} freeze           Disable to prevent object freeze when setting it to cache
   * @return {Cache} Cache
   */
  set(name, value, expiresInHours, freeze = true) {
    if (value !== undefined) {
      this._cache[name] = {
        expires: Date.now() + (expiresInHours || this._conf.dataExpireHours) *
                    3600 * 1000,
        data: freeze ? Object.freeze(value) : value,
      };
    }
    return this;
  }

  /**
   * This method should be used instead of get and set methods.
   * Returns promise which resolves with cached value
   * @param {Function} fn           Function whose result should be cached,
   *                                or some primitive value.
   *                                This function should return promise or primitive value
   * @param {string} name           Name for the data you cache
   * @param {number?} expiresInHours Overrides default value
   * @param {boolean?} freeze        Disable to prevent object freeze when setting it to cache
   * @return {Promise<Object>}
   */
  cached(fn, name, expiresInHours, freeze = true) {
    const cache = this.get(name);
    if (cache !== undefined) {
      return Promise.resolve(cache);
    }

    // cache promise so that same promise is returned to all calls made until cache is filled or promise rejected
    const maybePromise = fn();

    // checked this way so we include thenables & promises from bluebird
    if (typeof maybePromise?.then === 'function') {
      this.set(name, maybePromise, expiresInHours, false);
    } else {
      // not a promise, just add to cache and return
      this.set(name, maybePromise, expiresInHours, freeze);

      return maybePromise;
    }

    // if it's a promise but wasn't cached, await it, cache result and return
    // if it rejects it won't be cached
    return maybePromise.then((result) => {
      this.set(name, result, expiresInHours, freeze);

      return result;
    }).catch((err) => {
      // but we still need to clear the cached promise, so it doesn't keep rejecting for other calls
      this.invalidate(name);

      // and re-throw error so it's handled at the caller
      throw err;
    });
  }

  /**
   * Force data expiration
   * @param {string} name  Cache data name
   * @return {Cache} Cache
   */
  invalidate(name) {
    delete this._cache[name];
    return this;
  }

  /**
   * Invalidates whole cache, or only items which start with provided string
   * @param {string?} startsWith
   * @return {Cache}
   */
  invalidateAll(startsWith) {
    if (startsWith) {
      // partial invalidation
      this._cache = _.omitBy(this._cache, (_, name) =>
        name.startsWith(startsWith),
      );
    } else {
      this._cache = {}; // invalidate whole cache
    }
    return this;
  }

  /**
   * Cleans expired cache
   */
  _startCleaningThread() {
    if (this._threadId) {
      clearInterval(this._threadId);
    }

    if (!this._conf.checkEveryHours) {
      return;
    }

    this._threadId = setInterval(() => {
      const now = Date.now();
      this._cache = _.omitBy(this._cache, (c) => now > c.expires);
    }, Math.floor(this._conf.checkEveryHours * 3600 * 1000));
  }
}

module.exports = Cache;
