const Cache = require('./localCache');

// This is how it's imported in services already, so let's not break that

module.exports = {
  Cache: new Cache(),
};
