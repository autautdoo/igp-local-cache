const Cache = require('./localCache');

// can be used for certain type of tests
const cache = new Cache({
  checkEveryHours: 0, // disable clearing thread so Jest exits properly
});

beforeEach(() => {
  jest.clearAllMocks();
  cache.invalidateAll();
});

jest.useFakeTimers();
jest.spyOn(global, 'setInterval');
jest.spyOn(global, 'clearInterval');

test('`get` returns value set by `set`', () => {
  cache.set('testkey', 'testval');

  const cached = cache.get('testkey');

  expect(cached).toBe('testval');
});

test('`cached` returns cached value for sync functions', async () => {
  const callWhenUncached = jest.fn(() => {});

  await cache.cached(() => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  await cache.cached(() => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  expect(callWhenUncached).toHaveBeenCalledTimes(1);
});

test('`cached` returns cached value for async functions', async () => {
  const callWhenUncached = jest.fn(() => {});

  await cache.cached(async () => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  await cache.cached(async () => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  expect(callWhenUncached).toHaveBeenCalledTimes(1);
});

test('`cached` returns same promise if called during promise\'s execution', async () => {
  const callWhenUncached = jest.fn(() => {});

  cache.cached(async () => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  cache.cached(async () => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  expect(callWhenUncached).toHaveBeenCalledTimes(1);
});

test('results from rejected promises are not cached & errors are thrown at the caller', async () => {
  const callWhenUncached = jest.fn(() => {});

  await expect(async () => {
    await cache.cached(async () => {
      callWhenUncached();

      throw new Error('test error');
    }, 'testkey');
  }).rejects.toThrow();

  await cache.cached(async () => {
    callWhenUncached();

    return 'testval';
  }, 'testkey');

  expect(callWhenUncached).toHaveBeenCalledTimes(2);
});

test('cleaning timer is started without explicit init', async () => {
  const cache = new Cache();

  expect(setInterval).toHaveBeenCalledTimes(1);

  clearInterval(cache._threadId);
});

test('cleaning timer is started with explicit init', async () => {
  const cache = new Cache();

  cache.config({dataExpireHours: 2});

  expect(setInterval).toHaveBeenCalledTimes(2);

  clearInterval(cache._threadId);
});

test('cleaning timer is cleared on config', async () => {
  const cache = new Cache();

  cache.config({dataExpireHours: 2});

  expect(clearInterval).toHaveBeenCalledTimes(1);

  clearInterval(cache._threadId);
});
